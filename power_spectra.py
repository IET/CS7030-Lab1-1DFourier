import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
#Good Reference: http://matplotlib.org/users/pyplot_tutorial.html

def rect(t):
	if abs(t) <= 0.5:
		return 0.5
	elif abs(t) > 0.5:
		return 0
		
def sinc(t):
	return np.sin(np.pi*t)/(np.pi*t)
	
def sgn(t):
	if t > 0:
		return 1
	if t == 0:
		return 0
	if t < 0:
		return -1
		

def tri(t):
	if abs(t) <= 1:
		return 1 - abs(t)
	else:
		return 0

def imp(t):
	if t == 0:
		return 1
	else:
		return 0
	
def comb(t):
	#Period of Dirac Delta. Probably shouldn'y hard-code this
	T = 0.5
	
	if t%T == 0:
		return 1
	else:
		return 0

def calc_power_spectra(signal, title):
	global fig_no
	fig, axarr = plt.subplots(2,2)
	fig.suptitle(title, fontsize=20)
	fig.subplots_adjust(hspace = 0.4)
	plt.ylim(-2, 2)
	
	axarr[0,0].set_title("Time Domain")
	axarr[0,0].set_xlabel("Time")
	axarr[0,0].set_ylabel("Amplitude")
	axarr[0,0].set_ylim(min(signal)-0.1,max(signal) + 0.1)
	axarr[0,0].plot(t, signal)
	
	rfft_output = np.fft.rfft(signal)

	magnitude_only = [np.sqrt(i.real**2 + i.imag**2)/len(rfft_output) for i in rfft_output]
	frequencies = freq = np.fft.fftfreq(len(signal), num_samples)
	
	axarr[1,0].set_title("Frequency Domain")
	axarr[1,0].set_xlabel("Frequency (in Hz)")
	axarr[1,0].set_ylabel("Amplitude")
	axarr[1,0].plot(rfft_output, 'r')
	
	inverse_transform = np.fft.ifft(rfft_output)
	
	axarr[0,1].set_title("Approximated Waveform")
	axarr[0,1].set_xlabel("Time")
	axarr[0,1].set_ylabel("Amplitude")
	axarr[0,1].plot(inverse_transform, 'r')
	
	pow_spec = np.array([abs(i)**2 for i in rfft_output])
	
	axarr[1,1].set_title("Power Spectrum")
	axarr[1,1].set_xlabel("|G(Ft)|^2")
	axarr[1,1].set_ylim(min(pow_spec)-0.1,max(pow_spec) + 0.1)
	axarr[1,1].plot(pow_spec, 'r')

	plt.savefig("{0} power spectrum.png".format(title), dpi=96)
	plt.show()

freq = 5 #hz - cycles per second
amplitude = 1
starting_point = -1.5
time_to_plot = 3 # second
sample_rate = 100 # samples per second
num_samples = sample_rate * time_to_plot
t = np.linspace(starting_point, starting_point + time_to_plot, (3)/0.01+1)

while True:
	index = input("Please Choose From the Following Options:\
					\n1. Rectangle\
					\n2. Sinc\
					\n3. Signum\
					\n4. Triangle\
					\n5. Impulse/Dirac Delta\
					\n6. Comb \
					\n7. Exit \n\n")
	

	if index == '1':
		calc_power_spectra([amplitude * rect(i) for i in t], "Rectangle Function")
	elif index == '2':
		#Sampling rate above works better for most functions but breaks this one.
		#The below changes sampling frequency temporarily as a workaround
		temp = t
		t = np.linspace(starting_point, starting_point + time_to_plot, num_samples)
		calc_power_spectra([amplitude * sinc(i) for i in t], "Sinc Function")
		t = temp
	elif index == '3':
		calc_power_spectra([amplitude * sgn(i) for i in t], "Signum Function")
	elif index == '4':
		calc_power_spectra([amplitude * tri(i) for i in t], "Triangle Function")
	elif index == '5':
		calc_power_spectra([amplitude * imp(i) for i in t], "Impulse Function")
	elif index == '6':
		calc_power_spectra([amplitude * comb(i) for i in t], "Comb Function")
	elif index == '7':
		break
	else:
		print("Invalid Input, Please Choose From the available Options\n")